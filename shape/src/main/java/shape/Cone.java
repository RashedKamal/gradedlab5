package shape;

public class Cone implements Shape3d {
    private double radius;
    private double height;

    public Cone(double radius, double height) {
        this.radius = radius;
        this.height = height;
    }

    @Override
    public double getVolume() {
        double volume = 3.14 * Math.pow(this.radius, 2) * (this.height / 3);
        return volume;
    }

    @Override
    public double getSurfaceArea() {
        double surfaceArea = 3.14 * this.radius * (this.radius + Math.sqrt(Math.pow(height, 2) + Math.pow(this.radius, 2)));
        return surfaceArea;
    }

    public double getRadious() {
        return this.radius;
    }

    public double getHeight() {
        return height;
    }
}
