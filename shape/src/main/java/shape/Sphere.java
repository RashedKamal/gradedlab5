// Rashed Kamal 2034147
package shape;

public class Sphere implements Shape3d {
    private double radius;

    public Sphere(double radius){
    this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }
    
    @Override
    public double getVolume() {
        // throw new UnsupportedOperationException("Not Written Yet");
        return (3.14*(Math.pow(radius, 3))); 
    }
    
    @Override
    public double getSurfaceArea() {
        // throw new UnsupportedOperationException("Not Written Yet");
        return (4*3.14*(Math.pow(radius, 2)));
    }
}