package shape;

public interface Shape3d {
    double getVolume();

    double getSurfaceArea();
}