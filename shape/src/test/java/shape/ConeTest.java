// Rashed Kamal 2034147
package shape;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ConeTest {

    @Test
    public void testConeVolume(){
        Cone cone = new Cone(2, 4);
        assertEquals("Bad Volume", 16.76, cone.getVolume(), 0.1);
    }

    @Test
    public void testConeArea(){
        Cone cone = new Cone(4, 5);
        assertEquals("Bad Surface Area",130.76, cone.getSurfaceArea(), 0.1);
    }

    @Test  
    public void testConeConstructor(){
        double radious = 5;
        double hight = 10;
        Cone cone = new Cone(radious, hight);
        assertEquals(radious, cone.getRadious(), 0.0001);
        assertEquals(hight , cone.getHeight(), 0.0001);
    }
}