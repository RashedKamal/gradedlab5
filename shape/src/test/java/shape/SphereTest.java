//Sina Saniei 2036857
package shape;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SphereTest {

    @Test
    public void testSphereVoloume() {
        Sphere sphere = new Sphere(2);
        assertEquals("Bad volume", 25.12, sphere.getVolume(), 0.1);

    }

    @Test
    public void testSphereSurface() {
        Sphere sphere = new Sphere(2);
        assertEquals("message", 50.26, sphere.getSurfaceArea(), 0.1);
    }

    @Test
    public void sphereConstructureTest() {
        double radious = 2;
        Sphere sphere = new Sphere(radious);
        assertEquals(radious, sphere.getRadius(), 0.0001);
    }
}
